<?php

namespace SOSVelo\Bundle\PointBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SOSVelo\Bundle\PointBundle\Entity\Point;
use SOSVelo\Bundle\PointBundle\Form\Type\PointFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Point controller that handle the CRUD
 *
 * @todo Use symfony form to get the form validations
 *
 * Class PointController
 * @package SOSVelo\Bundle\PointBundle\Controller
 */
class PointController extends Controller {

    /**
     * Add_point with form
     *
     * @Apidoc()
     * @Route("/add_point")
     */
    public function add_pointAction() {

        $em = $this->getDoctrine()->getManager();
        $pointRep = $em->getRepository("SOSVeloPointBundle:Point");
        $user = $this->container->get('security.context')->getToken()->getUser();
        $point = $pointRep->findOneByUser($user);

        $isNewPoint = ($point === null);

        if ($isNewPoint) {
            $point = new Point();
        }

        $form = $this->createForm(new PointFormType(), $point);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $point->setUser($user);
                $em->persist($point);
                $em->flush();
                
                // echo '<script type="text/javascript">alert("Votre nouveau point a été enregistrée. Il sera validé par l\'association avant l\'ajout."); window.location.href = "home"; </script> ';
                if ($isNewPoint) {
                    $request->getSession()->getFlashBag()->add('success', 'Votre nouveau point a été enregistrée. Il sera validé par l\'association avant l\'ajout.');
                } else {
                    $request->getSession()->getFlashBag()->add('success', 'Vos modifications ont bien été prises en compte.');
                }
                
                return $this->redirect($this->generateUrl('sosvelo_home'));
            }
        }

        $services = $this->getDoctrine()->getManager()->getRepository("SOSVeloPointBundle:PointService")->findName();

        return $this->render('SOSVeloPointBundle:Point:add_point.html.twig', array(
            'point' => $point,
                    'form' => $form->createView(),
                    'services' => $services
        ));
    }

    /**
     * Create an point with json
     *
     * @Apidoc()
     * @Route("/point/delete/{id}", name="sosvelo_delete_point")
     */
    public function delete_pointAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        
        $point = $em->getRepository("SOSVeloPointBundle:Point")->find($id);
        if (null === $point) {
            throw new NotFoundHttpException("Ce point n'existe pas.");
        }

        // On créé un formulaire vide afin d'avoir une protection CSRF sur la page
        $form = $this->createFormBuilder()->getForm();

        if ($form->handleRequest($request)->isValid()) {
            $em->remove($point);
            $em->flush();
            $session->getFlashBag()->add('info', "Le point a bien été supprimée.");

            // Je retourne à la page d'où je viens
            if ($session->get('referer') === null) {
                return $this->redirect($request->headers->get('referer'));
            } else {
                $referer = $session->get('referer');
                $request->getSession()->remove('referer');
                return $this->redirect($referer);
            }
        } else if ($session->get('referer') === null) {
            // J'enregistre la page depuis laquelle je viens
            // Cet enregistrement est important, car lorsqu'on arrivera ici
            // par la validation du formulaire, le referer fera reference à la
            // page de suppression (et non a celle d'avant, que l'on souhaite)
            $session->set('referer', $request->headers->get('referer'));
        }

        return $this->render('SOSVeloPointBundle:Point:delete_point.html.twig', array(
            'point' => $point,
            'form'   => $form->createView()
        ));
        
    }

    /**
     * Create an point with json
     *
     * @Apidoc()
     * @Route("/points")
     * @Method({"POST"})
     */
    public function newPointAction() {
        // Preparing the response
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // Retrieve the request
        $request = $this->get('request');

        // If the content is json
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);

            // Creating the entity
            $point = new Point();
            $point->setName($data["point"]["name"]);
            $point->setAddress($data["point"]["adress"]);
            $point->setDescription($data["point"]["description"]);
            $point->setLongitude($data["point"]["longitude"]);
            $point->setLatitude($data["point"]["latitude"]);
            $point->setRating($data["point"]["rating"]);
            $point->setActivated($data["point"]["activated"]);

            // Persist the point
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($point);
            $em->flush();

            $response->setStatusCode(201); //Created
        } else {
            $response->setStatusCode(406); // Not acceptable
        }

        return $response;
    }

    /**
     * Get all points
     *
     * @Apidoc()
     * @Route("/points")
     * @Method({"GET"})
     */
    public function getPointsAction() {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $points = $this->getDoctrine()
                ->getRepository('SOSVeloPointBundle:Point')
                ->findAllPrivate($this->get('security.authorization_checker')->isGranted("ROLE_ADAV_MEMBER"));

        $response = new Response($serializer->serialize(array("points" => $points), 'json'));
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        return $response;
    }

    /**
     * Get all points
     *
     * @Apidoc()
     * @Route("/geojson/points")
     * @Method({"GET"})
     */
    public function getGeojsonPointsAction(Request $request) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $param = $request->query->get('type');
        $search = $request->query->get('search');

        $em = $this->getDoctrine()->getEntityManager();

        $privateGranted = $this->get('security.authorization_checker')->isGranted("ROLE_ADAV_MEMBER") || $this->get('security.authorization_checker')->isGranted("ROLE_ADMIN");
        $result = $this->getDoctrine()->getManager()->getRepository("SOSVeloPointBundle:Point")->getPoints($param, $search, $privateGranted);


        if ($param === "ServiceType") {
            $points = $result;
            $result=array();
            foreach ($points as $key => $point) {
                foreach($point->getServices() as $service) {
                    if ($service->getName() === $search) {
                        $result[] = $point;
                    }
                }
            }      
        }
        

        $var = "var points = ";
        $points = array(
            'type' => 'FeatureCollection',
            'features' => array()
        );
        for ($i = 0; $i < count($result); $i++) {
            $feature = array(
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array($result[$i]->getLongitude(), $result[$i]->getLatitude())
                ),
                'type' => 'Feature',
                'properties' => array(
                    'id' => $result[$i]->getId(),
                    'name' => $result[$i]->getName(),
                    'address' => $result[$i]->getAddress(),
                    'description' => $result[$i]->getDescription(),
                    'activated' => $result[$i]->getActivated(),
                    'schedule' => $result[$i]->getScheduleString(),
                )
            );
            array_push($points['features'], $feature);
        }

        $response = new Response($var . $serializer->serialize($points, 'json'));
        $response->headers->set('Content-Type', 'application/json; charset=utf-8');

        return $response;
    }

    /**
     * Get the point with the id
     *
     * @Apidoc()
     * @Route("/points/{id}", requirements={"id" = "\d+"})
     * @Method({"GET"})
     */
    public function getPointAction($id) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $point = $this->getDoctrine()
                ->getRepository('SOSVeloPointBundle:Point')
                ->find($id);

        $response = new Response($serializer->serialize(array("point" => $point), 'json'));
        $response->headers->set('Content-Type', 'application/json');

        // If point not found
        if (sizeof($point) == 0) {
            $response->setStatusCode(404);
        }

        return $response;
    }

    /**
     * activate the point
     *
     * @Apidoc()
     * @Route("/points/activate/{id}", requirements={"id" = "\d+"})
     * @Method({"POST"})
     */
    public function activatePointAction($id) {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $point = $this->getDoctrine()
                ->getRepository('SOSVeloPointBundle:Point')
                ->find($id);

        // If not instance of point
        if (!$point instanceof Point) {
            $response->setStatusCode(404);
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $point->setActivated(true);
            $em->persist($point);
            $em->flush();

            $response->setStatusCode(202); // Accepted
        }

        return $response;
    }

    /**
     * Get the comments of the point with the id
     *
     * @Apidoc()
     * @Route("/points/{id}/comments", requirements={"id" = "\d+"})
     * @Method({"GET"})
     */
    public function getCommentsPointAction($id) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $point = $this->getDoctrine()
                ->getRepository('SOSVeloPointBundle:Point')
                ->find($id);

        $response = new Response($serializer->serialize(array("point" => $point), 'json'));
        $response->headers->set('Content-Type', 'application/json');

        // If point not found
        if (sizeof($point) == 0) {
            $response->setStatusCode(404);
        }

        return $response;
    }

    /**
     * Delete an point
     *
     * @Apidoc()
     * @Route("/points/{id}", requirements={"id" = "\d+"})
     * @Method({"DELETE"})
     */
    public function deletePointAction($id) {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $point = $this->getDoctrine()
                ->getRepository('SOSVeloPointBundle:Point')
                ->find($id);

        // If not instance of point
        if (!$point instanceof Point) {
            $response->setStatusCode(404);
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $em->remove($point);
            $em->flush();

            $response->setStatusCode(202); // Accepted
        }

        return $response;
    }

    /**
     * Update an point
     *
     * @Apidoc()
     * @Route("/points/{id}", requirements={"id" = "\d+"})
     * @Method({"PUT"})
     */
    public function updatePointAction($id) {
        //Preparing the response
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        // Retrieve the request
        $request = $this->get('request');

        // If the content is json
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);

            $point = $this->getDoctrine()
                    ->getRepository('SOSVeloPointBundle:Point')
                    ->find($id);

            // If point not found
            if (sizeof($point) == 0) {
                $response->setStatusCode(404); // Not found
            } else {
                // Creating the entity
                $point->setName($data["point"]["name"]);
                $point->setAddress($data["point"]["adress"]);
                $point->setDescription($data["point"]["description"]);
                $point->setLongitude($data["point"]["longitude"]);
                $point->setLatitude($data["point"]["latitude"]);
                $point->setRating($data["point"]["rating"]);
                $point->setActivated($data["point"]["activated"]);

                // Persist the point
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($point);
                $em->flush();

                $response->setStatusCode(202); // Accepted
            }
        } else {
            $response->setStatusCode(406); // Not acceptable
        }

        return $response;
    }

}
