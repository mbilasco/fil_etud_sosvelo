<?php

namespace SOSVelo\Bundle\PointBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface; 
use SOSVelo\Bundle\PointBundle\Entity\Point;
use SOSVelo\Bundle\PointBundle\Entity\ServiceType;
use SOSVelo\Bundle\PointBundle\Form\PointScheduleType;

class PointFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Nom : '))
            ->add('address', null, array('label' => 'Adresse : '))
            ->add('description', 'textarea', array('label' => 'Description : '))
            ->add('latitude', 'hidden')
            ->add('longitude', 'hidden')
            ->add('services','entity', array(
                'class' => 'SOSVeloPointBundle:PointService',
                'property' => 'name',
                'multiple' => true,
                'expanded' => true
            ))
            ->add('schedule', 'collection', array(
                'type' => new PointScheduleType(),
                'allow_add' => false,
                'allow_delete' => false,
                'by_reference' => true,
            ))
            ->add('type', 'choice', array(
                'choices' => array(
                    'Privé (Visible uniquement par les membres de l\'ADAV)' => 'Private',
                    'Publique (Visible par tout le monde)' => 'Public',
                ),
                'expanded' => true,
                'multiple' => false,
                'choices_as_values' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'sosvelo_add_point';
    }
}