<?php

namespace SOSVelo\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder
        ->add('roles', 'choice', array(
            'choices' => array(
                'Je suis membre de l\'ADAV' => 'ROLE_ADAV_MEMBER',
            ),
            'expanded' => true,
            'multiple' => true,
            'choices_as_values' => true,
            'label' => ' ',
            'required' => false
        ))
        ->add('firstname', null, array('required' => true))
        ->add('lastname', null, array('required' => true))
        ->add('member_number', 'text')
        ;
    }

    public function setDefaultOption(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('Default', 'Register')
        ));
    }

    public function getName() {
        return 'sosvelo_user_registration';
    }

}
