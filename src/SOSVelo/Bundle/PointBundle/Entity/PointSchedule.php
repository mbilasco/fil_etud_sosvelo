<?php

namespace SOSVelo\Bundle\PointBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointSchedule
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="SOSVelo\Bundle\PointBundle\Entity\PointScheduleRepository")
 *
 *
 * RAPPEL :
 *     AM = Matin
 *     PM = Après midi
 */
class PointSchedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="amstart", type="time", nullable=true)
     *
     * heure d'ouverture le matin (si amclose = false)
     */
    private $amstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="amend", type="time", nullable=true)
     *
     * heure de fermeture le matin (si amclose = false)
     */
    private $amend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="amclose", type="boolean")
     *
     * ouvert le matin ?
     */
    private $amclose;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pmstart", type="time", nullable=true)
     *
     * heure d'ouverture l'après midi (si pmclose = false)
     */
    private $pmstart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pmend", type="time", nullable=true)
     *
     * heure de fermeture l'après midi (si pmclose = false)
     */
    private $pmend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pmclose", type="boolean")
     *
     * ouvert l'après midi ?
     */
    private $pmclose;

    public function __construct()
    {
        $this->amclose = false;
        $this->pmclose = false;

        $this->amstart = $this->amend = $this->pmstart = $this->pmend = new \Datetime("00:00");
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amstart
     *
     * @param \DateTime $amstart
     *
     * @return PointSchedule
     */
    public function setAmstart($amstart)
    {
        $this->amstart = $amstart;

        return $this;
    }

    /**
     * Get amstart
     *
     * @return \DateTime
     */
    public function getAmstart()
    {
        return $this->amstart;
    }

    /**
     * Set amend
     *
     * @param \DateTime $amend
     *
     * @return PointSchedule
     */
    public function setAmend($amend)
    {
        $this->amend = $amend;

        return $this;
    }

    /**
     * Get amend
     *
     * @return \DateTime
     */
    public function getAmend()
    {
        return $this->amend;
    }

    /**
     * Set amclose
     *
     * @param boolean $amclose
     *
     * @return PointSchedule
     */
    public function setAmclose($amclose)
    {
        $this->amclose = $amclose;

        return $this;
    }

    /**
     * Get amclose
     *
     * @return boolean
     */
    public function getAmclose()
    {
        return $this->amclose;
    }

    /**
     * Set pmstart
     *
     * @param \DateTime $pmstart
     *
     * @return PointSchedule
     */
    public function setPmstart($pmstart)
    {
        $this->pmstart = $pmstart;

        return $this;
    }

    /**
     * Get pmstart
     *
     * @return \DateTime
     */
    public function getPmstart()
    {
        return $this->pmstart;
    }

    /**
     * Set pmend
     *
     * @param \DateTime $pmend
     *
     * @return PointSchedule
     */
    public function setPmend($pmend)
    {
        $this->pmend = $pmend;

        return $this;
    }

    /**
     * Get pmend
     *
     * @return \DateTime
     */
    public function getPmend()
    {
        return $this->pmend;
    }

    /**
     * Set pmclose
     *
     * @param boolean $pmclose
     *
     * @return PointSchedule
     */
    public function setPmclose($pmclose)
    {
        $this->pmclose = $pmclose;

        return $this;
    }

    /**
     * Get pmclose
     *
     * @return boolean
     */
    public function getPmclose()
    {
        return $this->pmclose;
    }

    /**
     * 
     */
    public function getString()
    {
        $string = "";

        if ($this->amclose && $this->pmclose) {
            return "Fermé toute la journée";
        }

        if ($this->amclose) {
            $string .= date_format($this->pmstart, "H")."h".date_format($this->pmstart, "m")." - ".date_format($this->pmend, "H")."h".date_format($this->pmend, "m");
        } else if ($this->pmclose) {
            $string .= date_format($this->amstart, "H")."h".date_format($this->amstart, "m")." - ".date_format($this->amend, "H")."h".date_format($this->amend, "m");
        } else {
            $string .= date_format($this->amstart, "H")."h".date_format($this->amstart, "m")." - ".date_format($this->amend, "H")."h".date_format($this->amend, "m");
            $string .= " et ";
            $string .= date_format($this->pmstart, "H")."h".date_format($this->pmstart, "m")." - ".date_format($this->pmend, "H")."h".date_format($this->pmend, "m");
        }

        return $string;
    }
}
