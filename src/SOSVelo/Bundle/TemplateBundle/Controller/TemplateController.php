<?php

namespace SOSVelo\Bundle\TemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;



class TemplateController extends Controller
{
    /**
     * @Route("/home", name="home")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $id = 'thread_id';

        if ($request->query->get('point_id') !== null) {
            $id = $request->query->get('point_id');
        } 

        $thread = $this->container->get('fos_comment.manager.thread')->findThreadById($id);
        if (null === $thread) {
            $thread = $this->container->get('fos_comment.manager.thread')->createThread();
            $thread->setId($id);
            $thread->setPermalink($request->getUri());

            // Add the thread
            $this->container->get('fos_comment.manager.thread')->saveThread($thread);
        }
        $comments = $this->container->get('fos_comment.manager.comment')->findCommentTreeByThread($thread);

        $services = $this->getDoctrine()->getManager()->getRepository("SOSVeloPointBundle:PointService")->findName();

        return array(
            'comments' => $comments,
            'thread' => $thread,
            'services' => $services,
        );
    }

    /**
     * @Route("/contact")
     * @Template()
     */
    public function contactAction()
    {
        return array();
    }

    /**
     * @Route("/generic")
     * @Template()
     */
    public function genericAction()
    {
        return array();
    }

}
