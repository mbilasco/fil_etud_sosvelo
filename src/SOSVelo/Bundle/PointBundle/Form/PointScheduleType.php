<?php

namespace SOSVelo\Bundle\PointBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PointScheduleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amclose', 'checkbox', array( 'required' => false, "label" => " "))
            ->add('amstart', 'time', array(
                'widget' => 'single_text'
            ))
            ->add('amend', 'time', array(
                'widget' => 'single_text'
            ))
            ->add('pmclose', 'checkbox', array( 'required' => false, "label" => " "))
            ->add('pmstart', 'time', array(
                'widget' => 'single_text'
            ))
            ->add('pmend', 'time', array(
                'widget' => 'single_text'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SOSVelo\Bundle\PointBundle\Entity\PointSchedule'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sosvelo_bundle_pointbundle_pointschedule';
    }
}
