<?php

namespace SOSVelo\Bundle\UserBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use FOS\MessageBundle\Model\ParticipantInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sos_user")
 */
class User extends BaseUser implements ParticipantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="SOSVelo\Bundle\PointBundle\Entity\Point", mappedBy="user")
     */
    private $point;

    /**
     * @ORM\Column(name="member_number", type="integer", nullable=true)
     */
    private $member_number;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        parent::__construct();
        
        $this->roles = array('ROLE_USER');
        $this->setEnabled(true);
    }

    /**
     * Set point
     *
     * @param \SOSVelo\Bundle\PointBundle\Entity\Point $point
     *
     * @return User
     */
    public function setPoint(\SOSVelo\Bundle\PointBundle\Entity\Point $point = null)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return \SOSVelo\Bundle\PointBundle\Entity\Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set memberNumber
     *
     * @param integer $memberNumber
     *
     * @return User
     */
    public function setMemberNumber($memberNumber)
    {
        $this->member_number = $memberNumber;

        return $this;
    }

    /**
     * Get memberNumber
     *
     * @return integer
     */
    public function getMemberNumber()
    {
        return $this->member_number;
    }
}
