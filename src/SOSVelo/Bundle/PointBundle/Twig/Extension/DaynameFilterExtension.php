<?php 
namespace SOSVelo\Bundle\PointBundle\Twig\Extension;

class DaynameFilterExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            "dayname" => new \Twig_Filter_Method($this, 'displayDayname')
        );
    }

    public function displayDayname($day) {
        $result = "";

        $matches = array();
        preg_match_all("#<label class=\"required\">(.*)</label>#", $day, $matches);

        $day = $matches[1][0];
        
        switch(ucfirst(strtolower($day))) {
            case "Lundi":
            case "Mardi":
            case "Mercredi":
            case "Jeudi":
            case "Vendredi":
            case "Samedi":
            case "Dimanche" :
                $result = ucfirst(strtolower($day));
                break;
            case 0 : $result = "Lundi"; break;
            case 1 : $result = "Mardi"; break;
            case 2 : $result = "Mercredi"; break;
            case 3 : $result = "Jeudi"; break;
            case 4 : $result = "Vendredi"; break;
            case 5 : $result = "Samedi"; break;
            case 6 : $result = "Dimanche"; break;
            default: $result = "???"; break;
        }

        return $result;
    }

    public function getName()
    {
        return 'dayname_extension';
    }
}